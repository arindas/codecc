[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/codeems/codecc)

# `codecc`
A compiler frontend for all context free languages.

## Description
`codecc` is a collection of lexing and several parsing algorithms. It contains libraries and
executable utlities to create lexers and parsers for a given context free grammar.

## Why `codecc`?
Conventional lexers and parsers generators such as `Lex` and `Yacc` respectively, generate code,
which has to be compiled to generate the required lexers and parsers. I wanted to implement a 
more data structure oriented approach with minimal compilation. This project is also a part of my
University Compiler Design course assignments, where I had been tasked to implement some of the
algorithms.

This project is more pedagogical than "production level" in nature. The aim of the project is to 
teach how compiler front-ends can be designed. It is also an excellent reference project for 
C/C++ project organization using `cmake`. Simplicty has been given paramount importance with the 
use of lucid yet simple to understand code, clear documentation and minimal dependencies.

(I even included a minimal unit-testing framework completely written from scratch.)

## TODO
The following algorithms are to be implemented:

 - [ ] Regex matching
 - [ ] Abstract syntax trees
 - [ ] `LL(1)` First and follow
 - [ ] `LL(1)` Parsing
 - [ ] `LR(0)` Parsing
 - [ ] `SLR(1)` Parsing
 - [ ] `CLR(1)` Parsing
 - [ ] `LALR(1)` Parsing
