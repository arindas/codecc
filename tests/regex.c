#include "tester.h"
#include "regex/expr.h"

#include <string.h>
#include <stdio.h>

#define NTESTS 1



result postfix_test() {
  int nexpr = 8;


  char * exprs[] = {
    "a+", "a*", "a?",
    "a|b", "ab",
    "a+b?",
    "a(a|b)", "ab|a"
  };

  char * pofxs[] = {
    "a+", "a*", "a?",
    "ab|", "ab.",
    "a+b?",
    "aab|.", "ab.a|"
  };

  for (int i = 0; i < nexpr; i++) {
    char * pfix = regex_inf2post(exprs[i]);

    if(strcmp(pfix, pofxs[i]) != 0)
      return FAIL;
  }

  return PASS; 
}

// result passing_test() { return PASS; }

test_fn tests[] = {
  postfix_test,
};

int main(int argc, char *argv[]) {
  return run_tests (tests, NTESTS);
}
