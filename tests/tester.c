#include <stdio.h>

#include "tester.h"

int run_tests(test_fn * tests, int ntests) {
  int passed_tests = 0;

  for (int i = 0; i < ntests; i++) {
    result res = tests[i]();
    if (res == PASS)
      passed_tests++;

    printf("Test %3d %s.\n", i,
        res == PASS? "passed": "failed");
  }

  printf("Tests passed: [%d/%d]\n",
      passed_tests, ntests);

  if (passed_tests == ntests)
    printf("All tests passed!\n");

  return !(passed_tests == ntests);
}
