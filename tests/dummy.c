#include "tester.h"

#define NTESTS 2

result failing_test() { return FAIL; }
result passing_test() { return PASS; }

test_fn tests[NTESTS] = {
  failing_test,
  passing_test
};

int main(int argc, char *argv[]) {
  return run_tests (tests, NTESTS);
}
