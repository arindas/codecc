#ifndef _TESTER_H
#define _TESTER_H_

typedef enum {
  PASS, FAIL
} result;

typedef result (*test_fn) ();

int run_tests(test_fn *, int);

#endif
